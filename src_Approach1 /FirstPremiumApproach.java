import java.util.*;

public class FirstPremiumApproach {
	
	public static double premiumCalculator(String gender,int age, Boolean[] trait)
	{
		double base = 5000;
		if (age<=40)
			base  += (0.1*base);
		
		base += (0.2*base);
		
		if(gender.equals("Male"))
		{
			base += (0.02*base);
		}
		
		for(int i=0;i<4;i++)
		{
			if(trait[i])
			{
				base += (0.01*base);
			}
		}
		for (int j=4;j<8;j++)
		{
			if (j!=6 && trait[j])
			{
				base+=(0.03*base);
			}
			if (j==6 && trait[j])
			{
				base-=(0.03*base);
			}
		}
		return base;
	}
	
public static void main(String[]args)
{
	Scanner input = new Scanner(System.in);
	String name = input.nextLine();
	String gender = input.nextLine();
	int age = input.nextInt();
	Boolean[] trait = new Boolean[8];
	String userName="";
	for (int index=0;index<8;index++)
	{
	 trait[index] = input.nextBoolean();
	}
	String[] serName = name.split("\\s");
	if (gender.equals("Male"))
	{
		userName = "Mr. "+ serName[serName.length-1];
	}
	if (gender.equals("Female"))
	{
		 userName = "Ms. "+ serName[serName.length-1];
	}
	double answer = premiumCalculator(gender,age,trait);
	System.out.println("Health Insurance Premium for"+ 
			 userName +": Rs. " + answer);
}
}
